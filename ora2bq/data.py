"""
https://cloud.google.com/bigquery/docs/pandas-gbq-migration#loading_a_pandas_dataframe_to_a_table
"""

import logging

import coloredlogs

import pandas as pd

from ora2bq.connect import OracleConnectionPool
from ora2bq.ddl import OracleTable


class SingleTableLoader:
    source: OracleTable

    def __init__(self, owner: str, table_name: str, connections: OracleConnectionPool):
        self.source = OracleTable(owner=owner, name=table_name)

        conn = connections.conn_pool.acquire()
        self.source._from_oracle(connection=conn)
        connections.conn_pool.release(conn)

    def load(self, connections: OracleConnectionPool):
        conn = connections.conn_pool.acquire()

        query = f"select * from {self.source.as_oracle}"
        logging.info(query)

        # for result in cursor.execute(query):
        #    logging.info(result)
        df_ora = pd.read_sql(query, con=conn)
        print(df_ora)
        # df_ora.to_gbq(destination_table='')

        connections.conn_pool.release(conn)


class MultiLoader:
    pass


if __name__ == '__main__':
    coloredlogs.install(level='INFO')

    ora_conns = OracleConnectionPool('localhost/XE', 'ora2bq', 'ora2bq')

    stl = SingleTableLoader(owner='DWH', table_name='PART', connections=ora_conns)
    logging.info(stl.source)
    stl.load(ora_conns)

    ora_conns.conn_pool.close()
