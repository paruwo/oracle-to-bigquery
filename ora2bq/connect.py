import logging

import coloredlogs
import cx_Oracle
import pandas
import pandas_gbq


class OracleConnectionPool:
    dsn: str
    user: str
    password: str
    encoding: str = 'UTF-8'
    conn_pool: cx_Oracle.SessionPool

    @staticmethod
    def _init_session(connection, requested_tag):
        logging.debug('opening new connection')
        cursor = connection.cursor()
        cursor.execute("ALTER SESSION SET NLS_DATE_FORMAT = 'YYYY-MM-DD HH24:MI:SS'")

    def __init__(self, data_source_name: str, user: str, password: str):
        # https://cx-oracle.readthedocs.io/en/latest/user_guide/connection_handling.html#connection-pooling
        self.dsn = data_source_name
        self.user = user
        self.password = password

        logging.debug('creating Oracle connection pool')
        self.conn_pool = cx_Oracle.SessionPool(user=self.user, password=self.password, dsn=self.dsn,
                                               min=1, max=10, session_callback=self._init_session,
                                               increment=1, encoding=self.encoding)
        logging.info('Oracle connection pool to "%s" established', self.dsn)

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        logging.info('cleaning up Oracle connection pool')
        self.conn_pool.close()


class BigQueryConnection:
    pass


if __name__ == '__main__':
    coloredlogs.install(level='INFO')

    table_id = 'dwh.test'
    project_id = 'ora2bq'

    with OracleConnectionPool('localhost/XE', 'ora2bq', 'ora2bq') as ora_con:
        conn = ora_con.conn_pool.acquire()
        sql = f"select * from {table_id}"
        df = pandas.read_sql(sql, con=conn)
        ora_con.conn_pool.release(conn)

    # any test data frame from examples
    #df = pandas.DataFrame(
    #    {
    #        "my_string": ["a", "b", "c"],
    #        "my_int64": [1, 2, 3],
    #        "my_float64": [4.0, 5.0, 6.0],
    #        "my_bool1": [True, False, True],
    #        "my_bool2": [False, True, False],
    #        "my_dates": pandas.date_range("now", periods=3),
    #    }
    #)

    # for now, just append
    pandas_gbq.to_gbq(df, table_id, project_id=project_id, if_exists='append')
