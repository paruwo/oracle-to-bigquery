"""
DDL / structure-related module to map Oracle to BigQuery objects.
Intention is to extract Oracle structures and re-create compatible BigQuery objects in the given project.
"""

import logging
from abc import abstractmethod, ABC
from dataclasses import dataclass
from enum import Enum
from typing import List

import coloredlogs
from cx_Oracle import Connection
from google.cloud.bigquery import Table, SchemaField

from ora2bq.config import Config
from ora2bq.connect import OracleConnectionPool


@dataclass
class DataType(ABC):
    """A generic data type container."""
    type: str
    precision: int = None
    scale: int = 0


class BigQueryDataType(DataType):
    """A BigQuery-specific data type container."""

    def __str__(self):
        """String representation of BigQuery data type container."""
        if self.precision and self.scale != 0:
            return self.type + '(' + str(self.precision) + ', ' + str(self.scale) + ')'
        if self.precision and self.scale == 0:
            return self.type + '(' + str(self.precision) + ')'
        return self.type


class OracleDataType(DataType):
    """An Oracle-specific data type container."""

    def as_bigquery(self) -> BigQueryDataType:
        """Converts Oracle data type to compatible BigQuery data type."""
        logging.debug('to map: %s', self)
        type_mappings = Config().type_mappings

        # map aliases to base types
        aliases = type_mappings['aliases']
        mapped_type = self.type
        if self.type in aliases:
            mapped_type = aliases[self.type]
            logging.debug('normalized %s to %s', self.type, mapped_type)

        bq_type = None
        if self.precision is None and self.scale == 0:
            unparametrized = type_mappings['mapping']
            if mapped_type in unparametrized:
                bq_type = BigQueryDataType(unparametrized[mapped_type])
        else:
            # parametrized types, easier to implement instead of configure
            if mapped_type == 'NUMERIC':
                # "NUMERIC(<=18, 0)": "INT64"
                if self.precision <= 18 and self.scale == 0:
                    bq_type = BigQueryDataType('INT64')
                else:
                    bq_type = BigQueryDataType('NUMERIC', self.precision, self.scale)
            elif mapped_type in ('CHAR', 'NCHAR', 'VARCHAR2', 'NVARCHAR2'):
                bq_type = BigQueryDataType('STRING', self.precision)
            elif mapped_type == 'TIMESTAMP':
                bq_type = BigQueryDataType('DATETIME')
            elif mapped_type == 'TIMESTAMP WITH TIME ZONE':
                bq_type = BigQueryDataType('TIMESTAMP')

        logging.debug('mapped to %s', bq_type)
        if bq_type is None:
            raise ValueError(f'data type {self.type} not supported')

        return bq_type


@dataclass
class OracleColumn:
    """Oracle column container."""
    name: str
    type: OracleDataType
    required: bool = False

    def as_schema_field(self) -> SchemaField:
        """Converts Oracle column into BigQuery SchemaField object."""
        return SchemaField(name=self.name, field_type=str(self.type.as_bigquery()),
                           mode='REQUIRED' if self.required else 'NULLABLE',
                           precision=self.type.precision)


@dataclass
class OracleObjectSchema:
    """Container of Oracle schema, for instance of a table."""
    schema: List[OracleColumn]

    def __str__(self):
        return str(self.schema)

    def as_bigquery(self) -> List[SchemaField]:
        """Converts Oracle data type to compatible BigQuery list of SchemaField objects."""
        return [c.as_schema_field() for c in self.schema]


@dataclass
class OracleObject(ABC):
    """A generic Object database object."""
    owner: str
    name: str
    comment: str = None

    # def __init__(self, owner: str, name: str, comment: str = None):
    #    self.owner = owner
    #    self.name = name
    #    self.comment = comment

    @abstractmethod
    def _from_oracle(self, connection: Connection):
        """Determine all needed information from Oracle DB."""

    @abstractmethod
    def as_bigquery(self, project: str) -> Table:
        """Transforms Oracle representation to a BigQuery-compatible Storage API object."""


class OraclePartitioningType(Enum):
    """Oracle partitioning types."""
    RANGE: 1
    LIST: 2
    HASH: 3
    # REFERENCE: 4  # cannot be combined


class OraclePartitioning:
    """Container for Oracle partitioning configuration."""
    part_type: OraclePartitioningType
    part_sub_type: OraclePartitioningType = None  # not yet supported
    list_automatic: bool = False  # not yet supported
    interval: bool = False  # not yet supported, applicable for range-<any>
    interval_clause: str = None  # not yet supported

    def __init__(self, part_type: OraclePartitioningType):
        self.part_type = part_type
        # self.part_sub_type = part_sub_type

    def __str__(self):
        return str(self.part_type)

    # def _from_oracle(self):
    #    """Determine all needed information from Oracle DB."""


class OracleTable(OracleObject):
    """
    An Oracle table container.
    (Partitioning to be done later)
    """

    schema: OracleObjectSchema
    partitioning: OraclePartitioning = None

    def __init__(self, owner: str, name: str, comment: str = None, schema: OracleObjectSchema = None):
        super().__init__(owner=owner, name=name, comment=comment)
        self.schema = schema

    def __str__(self):
        return str(self.schema) + ' ' + str(self.partitioning)

    def _from_oracle(self, connection: Connection):
        cursor = connection.cursor()

        # comment
        for result in cursor.execute(Config().oracle_queries['tab-comment'], [self.owner, self.name]):
            self.comment = result[0]

        # schema
        object_schema: List[OracleColumn] = []
        for result in cursor.execute(Config().oracle_queries['tab-cols'], [self.owner, self.name]):
            # logging.debug(result)
            column_name = result[0]
            column_type = result[1]
            precision = int(result[3]) if result[3] else result[5]  # consider numeric / textual types
            scale = int(result[4])
            required = True if result[2] == 'N' else False

            odt = OracleDataType(type=column_type, precision=precision, scale=scale)
            object_schema.append(OracleColumn(name=column_name, type=odt, required=required))

        self.schema = OracleObjectSchema(object_schema)

    @property
    def as_oracle(self) -> str:
        return self.owner + '.' + self.name

    def as_bigquery(self, project: str) -> Table:
        """Transforms Oracle table representation to a BigQuery-compatible Storage API Table object."""
        table_ref = '.'.join([project, self.owner, self.name])
        table = Table(table_ref, schema=self.schema.as_bigquery())
        table.description = self.comment
        return table


class OracleView(OracleObject):
    """
    An Oracle view container.
    (UNSUPPORTED: column names + types + comments not supported)
    """

    select_query: str

    def __init__(self, owner: str, name: str, comment: str = None, select_query: str = None):
        super().__init__(owner=owner, name=name, comment=comment)
        self.select_query = select_query

    def as_bigquery(self, project: str) -> Table:
        """Transforms Oracle view representation to a BigQuery-compatible Storage API Table object."""
        table_ref = '.'.join([project, self.owner, self.name])
        view = Table(table_ref)
        view.view_query = self.select_query
        view.description = self.comment
        return view

    def _from_oracle(self, connection: Connection):
        cursor = connection.cursor()

        # comment
        for result in cursor.execute(Config().oracle_queries['tab-comment'], [self.owner, self.name]):
            self.comment = result[0]

        # sql query
        for result in cursor.execute(Config().oracle_queries['view-code'], [self.owner, self.name]):
            self.select_query = result[0]
            if result[1] >= 4000:
                logging.warning('view code for %s is too long (%i characters)', self.name, result[1])


if __name__ == '__main__':
    coloredlogs.install(level='INFO', milliseconds=True)

    with OracleConnectionPool('localhost/XE', 'ora2bq', 'ora2bq') as ora_con:
        t = OracleTable(owner='DWH', name='PART')

        conn = ora_con.conn_pool.acquire()
        t._from_oracle(connection=conn)
        ora_con.conn_pool.release(conn)

        logging.info(t.schema)

        bq_table = t.as_bigquery(project='bqproject')
        logging.info(bq_table)
        logging.info(bq_table.schema)

        v = OracleView(owner='DWH', name='V_PART')

        conn = ora_con.conn_pool.acquire()
        v._from_oracle(connection=conn)
        ora_con.conn_pool.release(conn)

        logging.info(v)
        # logging.info(v.select_query)

        bq_view = v.as_bigquery(project='bqproject')
        logging.info(bq_view)
