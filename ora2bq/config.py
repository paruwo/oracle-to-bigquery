"""
A global configuration container.
"""

import json
import logging
from pathlib import Path
from typing import Dict

import tomlkit
from py_singleton import singleton


@singleton
class Config:
    """The global configuration class."""
    project_root: Path
    type_mappings: Dict
    oracle_queries: Dict

    def __init__(self):
        self.project_root = Path(__file__).parents[1]

        type_mapping_file = self.project_root / 'ora2bq' / 'type_mapping.json'
        logging.debug('loading %s', type_mapping_file)
        with type_mapping_file.open('r') as mappings:
            self.type_mappings = json.load(mappings)

        queries_file = self.project_root / 'ora2bq' / 'queries.toml'
        logging.debug('loading %s', queries_file)
        with queries_file.open('r') as queries:
            self.oracle_queries = tomlkit.load(queries)
