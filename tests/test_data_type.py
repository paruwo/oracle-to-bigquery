from ora2bq.ddl import OracleDataType


def test_as_bigquery_int_unparametrized():
    t = OracleDataType('INT')
    assert t.as_bigquery().type == 'INT64'

    t = OracleDataType('INTEGER')
    assert t.as_bigquery().type == 'INT64'

    t = OracleDataType('SMALLINT')
    assert t.as_bigquery().type == 'INT64'


def test_as_bigquery_numeric_unparametrized():
    t = OracleDataType('DECIMAL')
    assert t.as_bigquery().type == 'NUMERIC'

    t = OracleDataType('DEC')
    assert t.as_bigquery().type == 'NUMERIC'

    t = OracleDataType('NUMERIC')
    assert t.as_bigquery().type == 'NUMERIC'

    t = OracleDataType('NUMBER')
    assert t.as_bigquery().type == 'NUMERIC'


def test_as_bigquery_float_unparametrized():
    t = OracleDataType('FLOAT')
    assert t.as_bigquery().type == 'FLOAT64'

    t = OracleDataType('BINARY_FLOAT')
    assert t.as_bigquery().type == 'FLOAT64'

    t = OracleDataType('BINARY_DOUBLE')
    assert t.as_bigquery().type == 'FLOAT64'

    t = OracleDataType('REAL')
    assert t.as_bigquery().type == 'FLOAT64'


def test_as_bigquery_date_unparametrized():
    t = OracleDataType('DATE')
    assert t.as_bigquery().type == 'DATETIME'

    t = OracleDataType('TIMESTAMP')
    assert t.as_bigquery().type == 'DATETIME'

    t = OracleDataType('TIMESTAMP WITH TIME ZONE')
    assert t.as_bigquery().type == 'TIMESTAMP'


def test_as_bigquery_lob_unparametrized():
    t = OracleDataType('CLOB')
    assert t.as_bigquery().type == 'STRING'

    t = OracleDataType('BLOB')
    assert t.as_bigquery().type == 'BYTES'


def test_as_bigquery_extended_unparametrized():
    t = OracleDataType('JSON')
    assert t.as_bigquery().type == 'JSON'

    t = OracleDataType('XMLType')
    assert t.as_bigquery().type == 'STRING'


def test_as_bigquery_numeric_parametrized():
    t = OracleDataType('NUMBER', 1)
    assert t.as_bigquery().type == 'INT64'

    t = OracleDataType('NUMERIC', 1, 0)
    assert t.as_bigquery().type == 'INT64'

    t = OracleDataType('NUMERIC', 18, 0)
    assert t.as_bigquery().type == 'INT64'

    t = OracleDataType('NUMERIC', 19, 0)
    assert t.as_bigquery().type == 'NUMERIC'
    assert t.as_bigquery().precision == 19
    assert t.as_bigquery().scale == 0


def test_as_bigquery_char():
    t = OracleDataType('CHAR', 1)
    assert t.as_bigquery().type == 'STRING'
    assert t.as_bigquery().precision == 1

    t = OracleDataType('NCHAR', 1)
    assert t.as_bigquery().type == 'STRING'
    assert t.as_bigquery().precision == 1

    t = OracleDataType('VARCHAR2', 1)
    assert t.as_bigquery().type == 'STRING'
    assert t.as_bigquery().precision == 1

    t = OracleDataType('NVARCHAR2', 1)
    assert t.as_bigquery().type == 'STRING'
    assert t.as_bigquery().precision == 1


def test_as_bigquery_timestamp_parametrized():
    t = OracleDataType('TIMESTAMP', 3)
    assert t.as_bigquery().type == 'DATETIME'
    assert t.as_bigquery().precision is None

    t = OracleDataType('TIMESTAMP WITH TIME ZONE', 3)
    assert t.as_bigquery().type == 'TIMESTAMP'
    assert t.as_bigquery().precision is None
