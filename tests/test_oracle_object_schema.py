import pytest
from google.cloud.bigquery import SchemaField

from ora2bq.ddl import OracleObjectSchema, OracleColumn, OracleDataType

schema = [
    OracleColumn('id', OracleDataType('NUMBER', 18, 0), True),
    OracleColumn('num', OracleDataType('NUMBER', 16, None), False),
    OracleColumn('num2', OracleDataType('NUMBER', 16, 4), False),
    OracleColumn('text', OracleDataType('VARCHAR2', 100, None), False),
    OracleColumn('inserted_at', OracleDataType('TIMESTAMP', 6, None), True)
]


@pytest.fixture
def test_easy(schema):
    oos = OracleObjectSchema(schema=schema)
    bqsl = oos.as_bigquery()

    assert bqsl == [
        SchemaField('id', 'INT64', 'REQUIRED'),
        SchemaField('num', 'NUMERIC(16)', 'NULLABLE'),  # , precision=16
        SchemaField('num2', 'NUMERIC', 'NULLABLE', precision=16, scale=4),
        SchemaField('text', 'STRING(100)', 'NULLABLE'),  # , max_length=100
        SchemaField('inserted_at', 'DATETIME', 'REQUIRED')
    ]
