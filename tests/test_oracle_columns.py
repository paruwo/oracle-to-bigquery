from google.cloud.bigquery import SchemaField

from ora2bq.ddl import OracleColumn, OracleDataType


def test_oracle_column_number_simple():
    oc = OracleColumn(name='col', type=OracleDataType('NUMBER'), required=False)
    bqsf = oc.as_schema_field()
    assert bqsf == SchemaField('col', 'NUMERIC', 'NULLABLE')


def test_oracle_column_number_advanced():
    oc = OracleColumn(name='col', type=OracleDataType('NUMBER', 18, 0), required=True)
    bqsf = oc.as_schema_field()
    assert bqsf == SchemaField('col', 'INT64', 'REQUIRED')
