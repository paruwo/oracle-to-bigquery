# oracle-to-bigquery

This is an attempt to migrate structures & data from Oracle to BQ with strong focus on data warehousing.

# Installation

From Code

```sh
git clone https://codeberg.org/paruwo/oracle-to-bigquery.git && cd oracle-to-bigquery
python3 -m ensurepip --user
pip3 install poetry --user
poetry install --no-dev

# for development or any other special purpose, feel free to install any extras
poetry install --extras "quality test"
```

Build Docker image

```sh
docker build -t paruwo/ora2bq .

# run small test
# docker run -it --rm --name oracle-to-bigquery -d paruwo/ora2bq
```

# Tests

```sh
poetry run python -m pytest
```



# Ideas

## Dev Setup

Install an Oracle test environment.
```shell
docker pull gvenzl/oracle-xe
docker run -d -p 1521:1521 --name oracle-xe --rm -e ORACLE_PASSWORD=sys gvenzl/oracle-xe

# restart docker if needed
sudo systemctl start docker.service
```

Install Oracle instant client. In PyCharm, extend your run configuration with environment variable
LD_LIBRARY_PATH=<path to instant client>.

## Structures

 * static schema transfer
   * determine structure
   * map to BigQuery
   * create in BigQuery
   * unsupported features
     * string list / hash partitioning
 * changing DDL?

## Data

 * check source/target structures on compatibility
 * full load of single tables / view
   * delete / insert
 * incremental loads
   * timestamp
   * numeric identifier

https://docs.gluent.com/4.0.x/goe/offload.html?highlight=varchar2#table-4-offload-default-data-type-mappings-oracle-database

# misc

```sql
-- original setup (fake just to have anything)
-- (run as sys as sysdba)
CREATE USER dwh IDENTIFIED BY "dwh";
GRANT CONNECT, resource, CREATE VIEW TO dwh;

CREATE USER ora2bq IDENTIFIED BY "ora2bq";
GRANT CONNECT, resource TO ora2bq;

ALTER USER dwh QUOTA UNLIMITED ON users;
ALTER USER ora2bq QUOTA UNLIMITED ON users;
```


```sql
-- (run as dwh)
-- fake any existing structures
CREATE TABLE dwh.test (id NUMBER, text varchar2(100 char));
COMMENT ON TABLE dwh.test IS 'test comment';
GRANT READ ON dwh.test TO ora2bq;

-- simple basic table
INSERT INTO dwh.test values (1, 'text');
COMMIT;
BEGIN
  dbms_stats.gather_table_stats('DWH', 'TEST', cascade => TRUE); 
END;

-- simple range-interval-partitioned table
CREATE TABLE dwh.part (id NUMBER NOT NULL, text varchar2(100 char), part_key NUMBER)
PARTITION BY RANGE(part_key)
INTERVAL(1)
(PARTITION p_init VALUES LESS THAN (0));
COMMENT ON TABLE dwh.part IS 'part comment';
GRANT READ ON dwh.part TO ora2bq;

INSERT INTO dwh.part values (1, 'text1', 0);
INSERT INTO dwh.part values (2, 'text2', 1);
COMMIT;
BEGIN
  dbms_stats.gather_table_stats('DWH', 'PART', cascade => TRUE); 
END;

CREATE VIEW dwh.v_part AS
SELECT id, text
  FROM dwh.part
 WHERE part_key >= 0;
COMMENT ON TABLE dwh.v_part IS 'v_part comment';
GRANT READ ON dwh.v_part TO ora2bq;
```
