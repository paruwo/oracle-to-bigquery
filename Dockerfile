
# https://hub.docker.com/_/python/?tab=description
# https://stackoverflow.com/questions/53835198/integrating-python-poetry-with-docker

FROM python:3.10-slim-bullseye

ENV PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    # poetry
    POETRY_NO_INTERACTION=1 \
    POETRY_VIRTUALENVS_CREATE=false \
    POETRY_CACHE_DIR='/var/cache/pypoetry' \
    POETRY_HOME='/usr/local'

# install poetry and clean
# # https://github.com/python-poetry/poetry
RUN apt-get update && apt-get upgrade -y && \
    apt-get install --no-install-recommends -y && \
    apt-get install -y curl && \
    # install poetry
    curl -sSL 'https://install.python-poetry.org' | python - && \
    poetry --version && \
    # cleanup
    apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false && \
    apt-get clean -y && rm -rf /var/lib/apt/lists/*

# copy essential app project files
WORKDIR /app
COPY pyproject.toml poetry.lock /app/

# hard installations
RUN mkdir -p /app/data/cache && \
    # poetry run pip install -U pip && \
    poetry install --no-interaction --no-ansi --no-dev && \
    rm -rf "$POETRY_CACHE_DIR"

# app code
COPY ora2bq/ /app/ora2bq/
# COPY OracleToBigQuery.py /app/

# run
CMD ["sh", "-c", \
     "python -V"]


## TODO
# https://stackoverflow.com/questions/58992954/install-oracle-instant-client-into-docker-container-for-python-cx-oracle
